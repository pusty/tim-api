<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:49
 */

namespace App\Controller;

use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use App\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class OrderController extends Controller
{
    /**
     * @Route("/order/status/add", name="order_status_add")
     * @Method({"POST"})
     */
    public function AddStatus(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $orderService->addStatus($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/order/status/delete", name="order_status_delete")
     * @Method({"POST"})
     */
    public function DeleteStatus(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $orderService->deleteStatus($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/order/add", name="order_add")
     * @Method({"POST"})
     */
    public function AddOrder(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $orderService->addOrder($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/order/delete", name="order_delete")
     * @Method({"POST"})
     */
    public function deleteOrder(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $orderService->deleteOrder($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/order/get/all", name="get_all_pc_order")
     * @Method({"GET"})
     */
    public function GetAllOrders(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['orders'] = $orderService->getAllOrders($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/order/mobile/all", name="get_all_user_orders")
     * @Method({"GET"})
     */
    public function GetMobileAllOrders(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['orders'] = $orderService->getMobileAllOrders($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/order/get/one", name="get_one_user_orders")
     * @Method({"GET"})
     */
    public function GetOrder(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['order'] = $orderService->getOrder($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/cars/mostwanted", name="cars_most_wanted_statistic")
     * @Method({"GET"})
     */
    public function CarsMostWantedLearn(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['cars'] = $orderService->getMostWantedCars();
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/cars/predict", name="cars_predict")
     * @Method({"GET"})
     */
    public function CarsPredict(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $orderService = new OrderService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['values'] = $orderService->CarsPredict();
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }


}