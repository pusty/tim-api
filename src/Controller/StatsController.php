<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 31.05.2018
 * Time: 12:30
 */

namespace App\Controller;

use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use App\Service\CarService;
use App\Service\statsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class StatsController extends Controller
{
    /**
     * @Route("/stats/cars", name="stats_cars")
     * @Method({"GET"})
     */
    public function userStatsCar(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $statsService = new statsService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['cars'] = $statsService->getCarsStats($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;
    }

    /**
     * @Route("/stats", name="stats_cars")
     * @Method({"GET"})
     */
    public function userStatsCount(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $statsService = new statsService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['count'] = $statsService->getStatCount($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;
    }
}