<?php

namespace App\Controller;

use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use App\Service\UsersService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UsersController extends Controller
{
    /**
     * @Route("/user/register", name="user_register")
     * @Method({"POST"})
     */
    public function registerUser(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $usersService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $usersService->saveUser($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/user/login", name="user_login")
     * @Method({"POST"})
     */
    public function loginUser(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $usersService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $usersService->loginUser($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/user/status/add", name="user_status_add")
     * @Method({"POST"})
     */
    public function AddStatus(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $userService->addStatus($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/user/status/delete", name="user_status_delete")
     * @Method({"POST"})
     */
    public function DeleteStatus(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $userService->deleteStatus($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/permissions/add", name="permission_add")
     * @Method({"POST"})
     */
    public function AddPermission(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $userService->addPermission($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/permissions/delete", name="permission_delete")
     * @Method({"POST"})
     */
    public function DeletePermission(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $userService->deletePermission($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/user/permissions/add", name="user_permissions_add")
     * @Method({"POST"})
     */
    public function AddUserPermission(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $userService->addUserPermission($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/user/permissions/delete", name="user_permissions_delete")
     * @Method({"POST"})
     */
    public function DeleteUserPermission(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $userService->deleteUserPermission($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/", name="home")
     * @Method({"POST", "GET"})
     */
    public function Home(Request $request)
    {
        $response = new ResponseErrors();

        $tab['status'] = $response->getMessage(803);

        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

    /**
     * @Route("/user/permissions/get", name="perms")
     * @Method({"POST", "GET"})
     */
    public function getPermissions(Request $request)
    {
        $response = new ResponseErrors();
        $em = $this->getDoctrine();
        $userService = new UsersService($em);
        $tab['status'] = $response->getMessage(802);
        try {
            $tab['permissions'] = $userService->getUserPermission($request);
        } catch (UsersExceptions $e) {
            $tab['status'] = $e->getMessage();
        }
        $response = new JsonResponse($tab, Response::HTTP_OK);
        return $response;

    }

}