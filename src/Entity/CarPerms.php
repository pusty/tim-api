<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 04.04.2018
 * Time: 18:03
 */

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="api_CarAccess")
 */
class CarPerms
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    private $userId;
    /**
     * @ORM\ManyToOne(targetEntity="CarClass")
     * @ORM\JoinColumn(name="classId", referencedColumnName="id")
     */
    private $classId;
    /**
     * @ORM\ManyToOne(targetEntity="CarType")
     * @ORM\JoinColumn(name="typeId", referencedColumnName="id")
     */
    private $typeId;
    /**
     * @ORM\ManyToOne(targetEntity="AccessStatus")
     * @ORM\JoinColumn(name="accessId", referencedColumnName="id")
     */
    private $statusId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getClassId()
    {
        return $this->classId;
    }

    /**
     * @param mixed $classId
     */
    public function setClassId($classId): void
    {
        $this->classId = $classId;
    }

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * @param mixed $typeId
     */
    public function setTypeId($typeId): void
    {
        $this->typeId = $typeId;
    }

    /**
     * @return mixed
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * @param mixed $statusId
     */
    public function setStatusId($statusId): void
    {
        $this->statusId = $statusId;
    }

}