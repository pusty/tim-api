<?php

namespace App\Repository;

class CarRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllCarsByType(array $carTypes)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->join('c.carType', 't')
            ->where('t.id in (:carTypes)')
            ->setParameters(
                ['carTypes' => $carTypes]
            );
        $results = $qb->getQuery()->getResult();
        if ($results) return $results;
        else return '';
    }

    public function findAllCarsByClass(array $carClass)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->join('c.carClass', 't')
            ->where('t.id in (:carClass)')
            ->setParameters(
                ['carClass' => $carClass]
            );
        $results = $qb->getQuery()->getResult();
        if ($results) return $results;
        else return '';
    }

}