<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 13.06.2018
 * Time: 23:04
 */

namespace App\Repository;


class OrderRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOccupiedCars($dateStart, $dateEnd)
    {
        $qb = $this->createQueryBuilder('o');
        $qb->where(':dateEnd >= o.dateFrom AND :dateEnd<= o.dateTo')
            ->orWhere(':dateStart >= o.dateFrom AND :dateStart <= o.dateTo')
            ->orWHERE(':dateEnd >= o.dateFrom AND :dateEnd <= o.dateTo AND :dateStart >= o.dateFrom AND :dateStart <= o.dateTo')
            ->setParameter(':dateStart', $dateStart)
            ->setParameter(':dateEnd', $dateEnd);
        $results = $qb->getQuery()->getResult();
        if ($results) return $results;
        else return false;
    }

    public function findMostWanted($userid)
    {
        $qb = $this->createQueryBuilder('o');
        $qb->select("c.carMark, c.carModel, count(IDENTITY(o.carId)) as counter");
        $qb->innerJoin('o.carId', 'c', 'WITH', 'c.id = o.carId');
        $qb->where('o.userId = :userId');
        $qb->setParameter('userId', $userid);
        $qb->groupBy('o.carId');
        $qb->orderBy('counter', 'DESC');
        $results = $qb->getQuery()->getResult();

        if ($results) return $results[0];
        else return false;
    }

    public function lastOrder(int $userId)
    {
        $date = new \DateTime('today');
        $qb = $this->createQueryBuilder('o');
        $qb
            ->where('o.userId = :userId')
            ->andWhere('o.dateTo <= :today')
            ->setParameters([
                'userId' => $userId,
                'today' => $date
            ])
            ->setMaxResults(1)
            ->orderBy('o.dateTo', 'DESC');
        $results = $qb->getQuery()->getResult();
        if ($results) return $results[0];
        else return false;
    }

    public function findCarsToPredict(){
        $conn = $this->getEntityManager()->getConnection();
        $cars = NULL;
        try{
            $sql = 'SELECT api_Cars.`car_mark`,`api_Cars`.`car_model`, api_Orders.date_from, count(api_Orders.id) as ile  FROM api_Cars
LEFT JOIN api_Orders on api_Orders.carId = api_Cars.id
WHERE MONTH(api_Orders.date_from) = MONTH(CURRENT_DATE())
GROUP BY api_Cars.car_mark, api_Cars.car_model, api_Orders.date_from
ORDER BY date_from, ile DESC';
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $cars = $stmt->fetchAll();

        }catch(\Exception $e){
            $cars = $e->getMessage();
        }
        return $cars;
    }
    public function mostWantedCars(){
        $conn = $this->getEntityManager()->getConnection();
        $cars = NULL;
        try{
            $sql = 'SELECT api_Cars.id, api_Cars.car_mark, api_Cars.car_model, api_CarType.code, YEAR(date_from) as "y", MONTH(date_from) as "m", count(api_Orders.carId) as "c" FROM api_Orders
LEFT JOIN api_Cars on api_Orders.carId = api_Cars.id
LEFT JOIN api_car_type_connect on api_Cars.id = api_car_type_connect.carId
LEFT JOIN api_CarType on api_car_type_connect.carType = api_CarType.id
GROUP BY api_Orders.carId, YEAR(date_from), MONTH(date_from)
ORDER BY y,m,c DESC';
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $cars = $stmt->fetchAll();

        }catch(\Exception $e){
            $cars = $e->getMessage();
        }

        return $cars;
    }
    public function carsPredict($cars){
        $licznik = 0;
        $mianownik = 0;
        $sumax = 0;
        $sumay = 0;
        $ile = 0;
        foreach($cars as $car){
            $sumax += $car['x'];
            $sumay += $car['y'];
            $ile++;
        }
        $sredniax = $sumax/$ile;
        $sredniay = $sumay/$ile;
        foreach($cars as $car){
            $licznik += ($car['x']-$sredniax)*($car['y']-$sredniay);
            $mianownik += pow(($car['x']-$sredniax),2);

        }
        $a = $licznik/$mianownik;
        $b = $sredniay - $a*$sredniax;
        $tab['a'] = $a;
        $tab['b'] = $b;
        return $tab;
    }
}
