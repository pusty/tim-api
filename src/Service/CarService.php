<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:42
 */

namespace App\Service;

use App\Entity\AccessStatus;
use App\Entity\Car;
use App\Entity\CarClass;
use App\Entity\CarPerms;
use App\Entity\CarType;
use App\Entity\Order;
use App\Entity\Users;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ManagerRegistry;

class CarService
{
    /** @var ManagerRegistry */
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function addClass(Request $request)
    {
        $response = new ResponseErrors();
        $car = new CarClass();
        if ($request->get('code') != null) {
            $car->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $car->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(CarClass::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($car);
            $em->flush();
            if (null == $car->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function deleteClass(Request $request)
    {
        $response = new ResponseErrors();

        $check = $this->doctrine->getRepository(CarClass::class)->findOneBy(['code' => $request->get('code')]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->persist($check);
            $em->flush();
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addType(Request $request)
    {
        $response = new ResponseErrors();
        $car = new CarType();
        if ($request->get('code') != null) {
            $car->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $car->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(CarType::class)->findOneBy(['code' => $request->get('code')]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->persist($car);
            $em->flush();
            if (null == $car->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function deleteType(Request $request)
    {
        $response = new ResponseErrors();

        $check = $this->doctrine->getRepository(CarType::class)->findOneBy(['code' => $request->get('code')]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->remove($check);
            $em->flush();

        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addCar(Request $request)
    {
        $response = new ResponseErrors();
        $car = new Car();
        if ($request->get('year') != null) {
            $car->setProductionYear($request->get('year'));
        } else throw new UsersExceptions($response->getMessage(830));
        if ($request->get('fuel') != null) {
            $car->setFuel($request->get('fuel'));
        } else throw new UsersExceptions($response->getMessage(831));
        if ($request->get('engine') != null) {
            $car->setEngineCapacity($request->get('engine'));
        } else throw new UsersExceptions($response->getMessage(832));
        if ($request->get('transmission') != null) {
            $car->setEngineCapacity($request->get('transmission'));
        } else throw new UsersExceptions($response->getMessage(833));
        if ($request->get('seats') != null) {
            $car->setSeats($request->get('seats'));
        } else throw new UsersExceptions($response->getMessage(834));
        $car->setActive(true);
        if ($request->get('type') != null) {
            $car->setCarType($this->doctrine->getRepository(CarType::class)->findOneBy(['code' => $request->get('type')]));
        } else throw new UsersExceptions($response->getMessage(835));
        if ($request->get('class') != null) {
            $car->setCarClass($this->doctrine->getRepository(CarClass::class)->findOneBy(['code' => $request->get('class')]));
        } else throw new UsersExceptions($response->getMessage(836));
        $em = $this->doctrine->getManager();
        $em->persist($car);
        $em->flush();
        if (null == $car->getId()) {
            throw new UsersExceptions($response->getMessage(803));
        }
    }

    public function carInfo(Request $request)
    {
        $car = array();
        $info = $this->doctrine->getRepository(Car::class)->findOneBy(['id' => $request->get('id')]);
        $car['info'] = $info;
        if ($info) {
            foreach ($info['carType'] as $type) {
                $car['type'][] = $this->doctrine->getRepository(CarType::class)->findOneBy(['id' => $type]);
            }
            foreach ($info['carClass'] as $class) {
                $car['class'][] = $this->doctrine->getRepository(CarClass::class)->findOneBy(['id' => $class]);
            }
        }
        return $car;
    }

    public function getCarAccess(Request $request)
    {
        $access = array();
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        $info = $this->doctrine->getRepository(CarPerms::class)->findBy(['userId' => $user->getId()]);
        foreach ($info as $item) {
            $carClass = $this->doctrine->getRepository(CarClass::class)->findOneBy(['id' => $item->getClassId()]);
            $carType = $this->doctrine->getRepository(CarType::class)->findOneBy(['id' => $item->getTypeId()]);
            $status = $this->doctrine->getRepository(AccessStatus::class)->findOneBy(['id' => $item->getStatusId()]);
            $access[] = array(
                'carClass' => $carClass->getDescription(),
                'carType' => $carType->getDescription(),
                'accessStatus' => $status->getDescription()
            );
        }
        return $access;
    }

    public function getActiveCars(Request $request)
    {
        $response = new ResponseErrors();
        if ($request->get('login') == NULL) throw new UsersExceptions($response->getMessage(810));
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        $accesses = $this->doctrine->getRepository(CarPerms::class)->findBy(['userId' => $user->getId()]);
        $accessTypes = [];
        $accessClasess = [];
        foreach ($accesses as $access) {
            $accessTypes[] = $access->getTypeId();
            $accessClasess[] = $access->getClassId();
        }
        $types = $this->doctrine->getRepository(Car::class)->findAllCarsByType($accessTypes);
        $clasess = $this->doctrine->getRepository(Car::class)->findAllCarsByClass($accessClasess);
        $cars = array();
        foreach ($types as $type) {
            foreach ($clasess as $class) {
                if ($type->getId() == $class->getId()) {
                    $cars[] = array(
                        'id' => $type->getId(),
                        'carMark' => $type->getCarMark(),
                        'carModel' => $type->getCarModel(),
                        'productionYear' => $type->getProductionYear(),
                        'fuel' => $type->getFuel(),
                        'engineCapacity' => $type->getEngineCapacity(),
                        'transmission' => $type->getTramsission(),
                        'seats' => $type->getSeats()
                    );
                }
            }
        }
        return $cars;
    }

    public function getOccupiedCars(Request $request)
    {
        $response = new ResponseErrors();
        $start = $request->get('startDate');
        $end = $request->get('endDate');
        if ($start == NULL) throw new UsersExceptions($response->getMessage(870));
        if ($end == NULL) throw new UsersExceptions($response->getMessage(871));
        /** @var OrderRepository $orderRepository */
        $orderRepository = $this->doctrine->getRepository(Order::class);
        $cars = $this->doctrine->getRepository(Car::class)->findby(['active' => 1]);
        $occupiedCars = $orderRepository->findOccupiedCars($start, $end);
        if ($occupiedCars) {
            foreach ($cars as $key => $car) {
                foreach ($occupiedCars as $occupied) {
                    if ($occupied->getCarId()->getId() == $car->getId()) unset($cars[$key]);
                }
            }
        }
        $auta = array();
        foreach ($cars as $auto) {
            $auta[] = array(
                'id' => $auto->getId(),
                'mark' => $auto->getCarMark(),
                'model' => $auto->getCarModel(),
                'productionYear' => $auto->getProductionYear(),
                'fuel' => $auto->getFuel(),
                'engineCapacity' => $auto->getEngineCapacity(),
                'transmission' => $auto->getTramsission(),
                'seats' => $auto->getSeats()
            );
        }
        return $auta;
    }

}