<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 06.04.2018
 * Time: 10:51
 */

namespace App\Service;

use App\Entity\Car;
use App\Entity\CarClass;
use App\Entity\CarType;
use App\Entity\Order;
use App\Entity\OrderStatus;
use App\Entity\Users;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use Symfony\Component\HttpFoundation\Request;

class OrderService
{
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function addStatus(Request $request)
    {
        $response = new ResponseErrors();
        $order = new OrderStatus();
        if ($request->get('code') != null) {
            $order->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $order->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($order);
            $em->flush();
            if (null == $order->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function deleteStatus(Request $request)
    {
        $response = new ResponseErrors();

        $check = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['code' => $request->get('code')]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->remove($check);
            $em->flush();

        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addOrder(Request $request)
    {
        $response = new ResponseErrors();
        $order = new Order();
        if ($request->get('start') != null) {
            $order->setDateFrom(new \DateTime($request->get('start')));
        } else throw new UsersExceptions($response->getMessage(870));
        if ($request->get('end') != null) {
            $order->setDateTo(new \DateTime($request->get('end')));
        } else throw new UsersExceptions($response->getMessage(871));
        $status = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['code' => 'ACTIVE']);
        $order->setStatusId($status);
        if ($request->get('user') != null) {
            $order->setUserId($this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('user')]));
        } else throw new UsersExceptions($response->getMessage(810));
        if ($request->get('car') != null) {
            $order->setCarId($this->doctrine->getRepository(Car::class)->findOneBy(['id' => $request->get('car')]));
        } else throw new UsersExceptions($response->getMessage(873));
        $check = $this->doctrine->getRepository(Order::class)->findOneBy([
            'dateFrom' => new \DateTime($request->get('start')),
            'dateTo' => new \DateTime($request->get('end')),
            'userId' => $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('user')])
        ]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($order);
            $em->flush();
            if (null == $order->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(874));
    }

    public function deleteOrder(Request $request)
    {
        $response = new ResponseErrors();

        $check = $this->doctrine->getRepository(Order::class)->findOneBy([
            'dateFrom' => $request->get('start'),
            'dateTo' => $request->get('end'),
            'userId' => $request->get('user')
        ]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->persist($check);
            $em->flush();

        } else throw new UsersExceptions($response->getMessage(874));
    }

    public function getAllOrders(Request $request)
    {
        $response = new ResponseErrors();
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        if ($user) {
            $orders = $this->doctrine->getRepository(Order::class)->findBy(['userId' => $user->getId()], ['dateFrom' => 'DESC']);
            $i = 0;
            if ($orders) {
                foreach ($orders as $order) {
                    $status = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['id' => $order->getStatusId()]);
                    $car = $this->doctrine->getRepository(Car::class)->findOneBy(['id' => $order->getCarId()]);
                    $tab[$i] = array(
                        'dateFrom' => $order->getDateFrom(),
                        'dateTo' => $order->getDateTo(),
                        'car' => array(
                            'mark' => $car->getCarMark(),
                            'model' => $car->getCarModel(),
                            'production' => $car->getProductionYear(),
                            'fuel' => $car->getFuel(),
                            'capacity' => $car->getEngineCapacity(),
                            'transmission' => $car->getTramsission(),
                            'seats' => $car->getSeats()
                        ),
                        'status' => $status->getDescription(),
                    );
                    $i++;
                }
                return $tab;
            } else return '';

        } else throw new UsersExceptions($response->getMessage(810));

    }

    public function getMobileAllOrders(Request $request)
    {
        $response = new ResponseErrors();
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        if ($user) {
            $orders = $this->doctrine->getRepository(Order::class)->findBy(['userId' => $user->getId()]);
            $i = 0;
            if ($orders) {
                foreach ($orders as $order) {
                    $status = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['id' => $order->getStatusId()]);
                    $car = $this->doctrine->getRepository(Car::class)->findOneBy(['id' => $order->getCarId()]);
                    $tab[$i] = array(
                        'dateFrom' => $order->getDateFrom()->format('Y-m-d H:i:s'),
                        'dateTo' => $order->getDateTo()->format('Y-m-d H:i:s'),
                        'car' => array(
                            'mark' => $car->getCarMark(),
                            'model' => $car->getCarModel()
                        ),
                    );
                    $i++;
                }
                return $tab;
            } else return '';

        } else throw new UsersExceptions($response->getMessage(810));

    }

    public function getOrder(Request $request)
    {
        $response = new ResponseErrors();
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        if ($user) {
            $order = $this->doctrine->getRepository(Order::class)->findOneBy(['userId' => $user->getId(), 'id' => $request->get('orderId')]);
            $i = 0;
            if ($order) {
                $status = $this->doctrine->getRepository(OrderStatus::class)->findOneBy(['id' => $order->getStatusId()]);
                $car = $this->doctrine->getRepository(Car::class)->findOneBy(['id' => $order->getCarId()]);
                $cartype = $this->doctrine->getRepository(CarType::class)->findOneBy(['id' => $car->getCarType()]);
                $carstatus = $this->doctrine->getRepository(CarClass::class)->findOneBy(['id' => $car->getCarClass()]);
                $tab[$i] = array(
                    'dateFrom' => $order->getDateFrom(),
                    'dateTo' => $order->getDateTo(),
                    'car' => array(
                        'carType' => $cartype->getDescription(),
                        'carStatus' => $carstatus->getDescription(),
                        'production' => $car->getProductionYear(),
                        'fuel' => $car->getFuel(),
                        'capacity' => $car->getEngineCapacity(),
                        'transmission' => $car->getTransmission(),
                        'seats' => $car->getSeats()
                    ),
                    'status' => $status->getDescription(),
                );
                $i++;

                return $tab;
            }

        } else throw new UsersExceptions($response->getMessage(810));

    }

    public function getMostWantedCars(){
        $orderRepository = $this->doctrine->getRepository(Order::class);
        $tab['cars'] = $orderRepository->mostWantedCars();
        return $tab;
    }
    public function carsPredict(){
        $orderRepository = $this->doctrine->getRepository(Order::class);
        $cars = $orderRepository->findCarsToPredict();
        $tempcars = array();
        $predictcars = array();

        foreach($cars as $car){
            $predictcars[] = array(
                'carMark' => $car['car_mark'],
                'carModel' => $car['car_model'],
                'x' => substr($car['date_from'],8,2),
                'y' => $car['ile']
            );
        }
        //// TU SIĘ ZACZYNA KOD DO WYŚWIETLENIA AUT I ICH PUNKTÓW
        $listaaut = array();
        $i = 0;
        foreach($cars as $car) {
            if ($i == 0) {
                $listaaut[] = array(
                    'carMark' => $car['car_mark'],
                    'carModel' => $car['car_model'],
                    'x' => array(),
                    'y' => array($car['ile']),
                );
                $listaaut[0]['x'][] = substr($car['date_from'], 8, 2);
                $listaaut[0]['y'][] = $car['ile'];
            } else {
                $isset = false;
                $j = 0;
                foreach ($listaaut as $list) {
                    if ($list['carMark'] == $car['car_mark'] && $list['carModel'] == $car['car_model']) {
                        $key = array_search($list, $listaaut);
                        $list[$key]['x'][] = substr($car['date_from'], 8, 2);
                        $isset = true;
                    }
                    $j++;
                }
                if (!$isset) {
                    $listaaut[] = array(
                        'carMark' => $car['car_mark'],
                        'carModel' => $car['car_model'],
                        'x' => array(),
                        'y' => array(),
                    );
                    $listaaut[$j]['x'][] = substr($car['date_from'], 8, 2);
                    $listaaut[$j]['y'][] = $car['ile'];
                }
            }
            $i++;
        }
            /// TU SIĘ KOńCZY TEN KOD
            //////////////////////////////////////
            /// TUTAJ MÓJ STARY KOD Z KLUCZAMI
        foreach($cars as $car) {
            $carlist[] = array(
                'carMark' => $car['car_mark'],
                'carModel' => $car['car_model'],
                'x' => array(),
                'y' => array(),
            );
            if(array_key_exists($car['car_mark'].$car['car_model'],$tempcars)){
                $tempcars[$car['car_mark'].$car['car_model']]['x'][] = substr($car['date_from'],8,2);
                $tempcars[$car['car_mark'].$car['car_model']]['y'][] = $car['ile'];
            } else {
                $tempcars[$car['car_mark'].$car['car_model']] = array(
                    'carMark' => $car['car_mark'],
                    'carModel' => $car['car_model'],
                    'x' => array(),
                    'y' => array()
                );
                $tempcars[$car['car_mark'].$car['car_model']]['x'][] = substr($car['date_from'],8,2);
                $tempcars[$car['car_mark'].$car['car_model']]['y'][] = $car['ile'];
            }


        }
        ///////////////////////////////////////
        $tab['predict'] = $orderRepository->carsPredict($predictcars);
        $tab['cars'] = $predictcars;

        return $tab;
    }
}