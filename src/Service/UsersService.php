<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 28.03.2018
 * Time: 22:23
 */

namespace App\Service;

use App\Entity\Permissions;
use App\Entity\UserPerms;
use App\Entity\Users;
use App\Entity\UserStatus;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use Symfony\Component\HttpFoundation\Request;

class UsersService
{

    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function saveUser(Request $request)
    {
        $response = new ResponseErrors();
        $user = new Users();
        if ($request->get('login') != null) {
            $user->setLogin($request->get('login'));
        } else throw new UsersExceptions($response->getMessage(810));
        if ($request->get('password') != null) {
            $user->setPassword(md5($request->get('password')));
        } else throw new UsersExceptions($response->getMessage(811));
        if ($request->get('email') != null) {
            $user->setEmail($request->get('email'));
        } else throw new UsersExceptions($response->getMessage(812));
        $user->setActive(true);
        $user->setAction($this->doctrine->getRepository(UserStatus::class)->findOneBy(['code' => $request->get('status')]));
        $check = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if (null == $user->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else {
            $em = $this->doctrine->getManager();
            $em->flush();
        }

    }

    public function deleteUser(Request $request)
    {
        $response = new ResponseErrors();
        if ($request->get('login') != null) {
            $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
            $em = $this->doctrine->getManager();
            $em->remove($user);
            $em->flush();
        } else throw new UsersExceptions($response->getMessage(810));
    }

    public function getUsers()
    {
        $users = $this->doctrine->getRepository(Users::class)->findAll();
        $userzy = array();
        foreach ($users as $user) {
            $userzy[] = array(
                'login' => $user->getLogin(),
                'email' => $user->getEmail(),
                'active' => $user->getActive(),
            );
        }
        return $userzy;
    }

    public function loginUser(Request $request)
    {
        $response = new ResponseErrors();
        if ($request->get('login') == null) throw new UsersExceptions($response->getMessage(810));
        if ($request->get('password') == null) throw new UsersExceptions($response->getMessage(811));
        $user = $this->doctrine->getRepository(Users::class)->findOneBy([
                'login' => $request->get('login'),
                'password' => md5($request->get('password'))]
        );
        if (!$user) throw new UsersExceptions($response->getMessage(815));

    }

    public function addStatus(Request $request)
    {
        $response = new ResponseErrors();
        $user = new UserStatus();
        if ($request->get('code') != null) {
            $user->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $user->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(UserStatus::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if (null == $user->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function deleteStatus(Request $request)
    {
        $response = new ResponseErrors();
        $check = $this->doctrine->getRepository(UserStatus::class)->findOneBy(['code' => $request->get('code')]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->remove($check);
            $em->flush();
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addPermission(Request $request)
    {
        $response = new ResponseErrors();
        $user = new Permissions();
        if ($request->get('code') != null) {
            $user->setCode($request->get('code'));
        } else throw new UsersExceptions($response->getMessage(820));
        if ($request->get('description') != null) {
            $user->setCode($request->get('description'));
        } else throw new UsersExceptions($response->getMessage(821));
        $check = $this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('code')]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if (null == $user->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function deletePermission(Request $request)
    {
        $response = new ResponseErrors();
        $check = $this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('code')]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->remove($check);
            $em->flush();
        } else throw new UsersExceptions($response->getMessage(822));
    }

    public function addUserPermission(Request $request)
    {
        $response = new ResponseErrors();
        $user = new UserPerms();
        if ($request->get('login') != null) {
            $user->setUserId($this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]));
        } else throw new UsersExceptions($response->getMessage(810));
        if ($request->get('permission') != null) {
            $perm = $this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('permission')]);
            if ($perm != null) $user->setPermissionId($perm);
            else throw new UsersExceptions($response->getMessage(823));
        } else throw new UsersExceptions($response->getMessage(813));
        $check = $this->doctrine->getRepository(UserPerms::class)->findOneBy(['userId' => $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]), 'permissionId' => $this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('code')])]);
        if (!$check) {
            $em = $this->doctrine->getManager();
            $em->persist($user);
            $em->flush();
            if (null == $user->getId()) {
                throw new UsersExceptions($response->getMessage(803));
            }
        } else throw new UsersExceptions($response->getMessage(823));
    }

    public function deleteUserPermission(Request $request)
    {
        $response = new ResponseErrors();
        $check = $this->doctrine->getRepository(UserPerms::class)->findOneBy(['userId' => $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]), 'permissionId' => $this->doctrine->getRepository(Permissions::class)->findOneBy(['code' => $request->get('permission')])]);
        if ($check) {
            $em = $this->doctrine->getManager();
            $em->remove($check);
            $em->flush();

        } else throw new UsersExceptions($response->getMessage(823));
    }

    public function getUserPermission(Request $request)
    {
        $response = new ResponseErrors();
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['id' => $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')])]);
        $check = $this->doctrine->getRepository(UserPerms::class)->findBy(['userId' => $user]);
        $permission = array();
        foreach ($check as $perm) {
            $perms = $this->doctrine->getRepository(Permissions::class)->findOneBy(['id' => $perm->getPermissionId()]);
            $permission[] = array('code' => $perms->getCode(), 'description' => $perms->getDescription());
        }
        return $permission;
    }
}
