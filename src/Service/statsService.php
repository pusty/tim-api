<?php
/**
 * Created by PhpStorm.
 * User: pusty
 * Date: 01.06.2018
 * Time: 21:19
 */

namespace App\Service;

use App\Entity\AccessStatus;
use App\Entity\Car;
use App\Entity\CarClass;
use App\Entity\CarPerms;
use App\Entity\CarType;
use App\Entity\Order;
use App\Entity\Users;
use App\Exceptions\ResponseErrors;
use App\Exceptions\UsersExceptions;
use App\Repository\OrderRepository;
use Symfony\Component\HttpFoundation\Request;

class statsService
{
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function getCarsStats(Request $request)
    {
        $cars = array();
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        $orders = $this->doctrine->getRepository(Order::class)->findBy(['userId' => $user->getId()]);
        foreach ($orders as $order) {
            if ($key = array_search($order->getCarId(), $cars)) {
                $cars[$key]['count'] += 1;
            } else {
                $stack = array(
                    'carId' => $order->getCarId(),
                    'count' => 0
                );
                array_push($stack, $cars);
            }

        }
        return $cars;
    }

    public function getStatCount(Request $request)
    {
        $response = new ResponseErrors();
        if ($request->get('login') == NULL) throw new UsersExceptions($response->getMessage(810));
        $user = $this->doctrine->getRepository(Users::class)->findOneBy(['login' => $request->get('login')]);
        $orders = $this->doctrine->getRepository(Order::class)->findBy(['userId' => $user->getId()]);
        $i = 0;
        if ($orders) {
            foreach ($orders as $order) {
                $i++;
            }
        }
        $j = 0;
        $orders1 = $this->doctrine->getRepository(Order::class)->findAll();
        foreach ($orders1 as $order1) {
            $j++;
        }
        $tab['user'] = $i;
        $tab['company'] = $j;
        $tab['percent'] = round($i / $j * 100, 2);
        $orderRepository = $this->doctrine->getRepository(Order::class);
        $cars = $orderRepository->findMostWanted($user->getId());
        if ($cars) {
            $tab['rentedCars'] = $cars;
        } else $tab['rentedCars'] = '';
        $lastOrder = $orderRepository->lastOrder($user->getId());
        if ($lastOrder) {
            $tab['lastRent'] = $lastOrder->getDateTo()->format('Y-m-d H:i:s');
        } else $tab['lastRent'] = '';

        return $tab;
    }

}